<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
	<div id="hero" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>);">
		<div class="section-banner">
			<div class="banner-content">
				<div class="banner-date"><?php echo mysql2date('j M. Y', $post->post_date); ?></div>
				<div class="banner-heading"><?php the_title(); ?></div>
			</div>
		</div>
	</div>

	<main class="section section-content" role="main">
		<?php the_content(); ?>
	</main>
<?php endwhile; ?>

<?php get_footer(); ?>
