<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
	<div id="hero" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>);">
		<div class="section-banner">
			<div class="banner-content">
				<div class="banner-date"><?php echo mysql2date('j M. Y', $post->post_date); ?></div>
				<div class="banner-heading"><?php the_title(); ?></div>
			</div>
		</div>
	</div>

	<main class="section section-content" role="main">
		<?php the_content(); ?>
	</main>

	<?php $author_id = get_post_meta($post->ID, 'hoc_post_author', true); ?>

	<?php if (!empty($author_id)) : ?>
		<div class="author">
			<div class="author-image">
				<img src="<?php echo get_the_post_thumbnail_url($author_id, 'thumbnail'); ?>" alt="">
			</div>
			<div class="author-content">
				<h2>Skrevet af</h2>
				<?php //echo get_user_meta($post->post_author, 'first_name', true).' '.get_user_meta($post->post_author, 'last_name', true); ?>
				<a href="<?php echo get_permalink($author_id); ?>"><?php echo get_the_title($author_id); ?></a>
			</div>
		</div>
	<?php endif; ?>

	<?php //the_sections($post->ID); ?>

	<?php the_section(array(
		'type' => 'grid',
		'post_type' => 'post',
		'columns' => 2,
		'posts' => 2
	)); ?>

	<div class="section">
		<div class="section-code">
			<p><strong>Kunne du lide vores indlæg?</strong> {</p>
			<p>Lad os tage en <span class="purple">snak</span> om hvordan vores <span class="orange">erfaringer</span> kan skabe <span class="green">værdi</span> for dig</p>
			<p>}</p>
		</div>
		<h2><a href="" class="button">Kontakt</a></h2>
	</div>
<?php endwhile; ?>

<?php get_footer(); ?>
