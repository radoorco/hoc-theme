<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,700" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<nav id="header-nav" class="header-nav" role="navigation">
	<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
		<?php if (empty(get_bloginfo('name'))) : ?>
			Replace logo here
		<?php else : ?>
			<?php bloginfo( 'name' ); ?>
		<?php endif; ?>
	</a>
	<?php
		wp_nav_menu(array(
			'theme_location' => 'primary',
			'depth' => 1,
			'container' => false,
			'fallback_cb' => 'wp_rc_navwalker::fallback',
			'walker' => new wp_rc_navwalker()
		));
	?>
	<ul class="single-menu">
		<?php
			$post_name = '';
			$next_post = get_next_post();
			$prev_post = get_previous_post();

			$parent_post = '';
			if ($post->post_type == 'project') {
				$parent_post = esc_url(home_url('/')).'/projekter/';
				$post_name = 'projekt';
			}
			if ($post->post_type == 'employee') {
				$parent_post = esc_url(home_url('/')).'/laer-os-at-kende/';
				$post_name = 'medarbejder';
			}
			if ($post->post_type == 'job') {
				$parent_post = esc_url(home_url('/')).'jobs-i-huset/';
				$post_name = 'opslag';
			}

			if (!empty($parent_post)) {
				echo '<li class="nav-item single-nav-close">';
					echo '<a class="nav-link" href="'.$parent_post.'">Luk</a>';
				echo '</li>';
			}

			if (!empty($prev_post)) {
				echo '<li class="nav-item single-nav-prev">';
					echo '<a class="nav-link" href="'.get_permalink($prev_post->ID).'">Forrige '.$post_name.'</a>';
				echo '</li>';
			}

			if (!empty($next_post)) {
				echo '<li class="nav-item single-nav-next">';
					echo '<a class="nav-link" href="'.get_permalink($next_post->ID).'">Næste '.$post_name.'</a>';
				echo '</li>';
			}
		?>
	</ul>
</nav>

<div id="mobile-toggle" class="mobile-toggle">
	<div class="mobile-lines">
		<div class="line1"></div>
		<div class="line2"></div>
		<div class="line3"></div>
		<div class="line4"></div>
		<div class="line5"></div>
		<div class="line6"></div>
	</div>
	<div class="mobile-burger">
		<div class="burger1"></div>
		<div class="burger2"></div>
		<div class="burger3"></div>
	</div>
</div>

<!--<div id="bodymovin"></div>-->
