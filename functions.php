<?php
require_once('inc/helpers.php');
require_once('inc/base.php');
require_once('inc/head.php');
require_once('inc/jquery-cdn.php');
require_once('inc/clean-up.php');
require_once('inc/query-vars.php');
require_once('inc/google-analytics.php');
require_once('inc/sidebar.php');
require_once('inc/search.php');
require_once('inc/widgets.php');
require_once('inc/wp_rc_navwalker.php');
require_once('inc/yoast.php');
require_once('inc/oembed.php');

if ( ! function_exists( 'rc_setup' ) ) :
	function rc_setup() {
		rc_theme_support();
	}
endif;
add_action( 'after_setup_theme', 'rc_setup' );

//Allow editor style.
add_action( 'admin_init', 'rc_custom_editor_styles' );

// clean ups
add_action( 'init', 'head_cleanup' );
add_action( 'init', 'rc_remove_emojicons' );
add_action( 'init', 'rc_rel_canonical' );

// head
add_action( 'wp_head','rc_add_favicon');
add_action( 'wp_head', 'address_mobile_address_bar' );
add_action( 'wp_head','rc_add_manifest');

// styles & scripts
add_action( 'wp_enqueue_scripts', 'rc_scripts' );
//add_action( 'wp_enqueue_scripts', 'rc_load_more_js' );

// cleaning up random code around images
add_filter( 'the_content', 'filter_ptags_on_images' );


add_action('init', function() {
	remove_post_type_support('page', 'editor');
});



function the_sections($id) {
	$sections = get_post_meta($id, 'sections', true);

	if (!empty($sections)) {
		foreach ($sections as $section) {
			the_section($section);
		}
	}
}

function the_section($section) {
	$type = isset($section['type']) ? $section['type'] : '';
	$heading = isset($section['heading']) ? $section['heading'] : '';
	$text = isset($section['text']) ? $section['text'] : '';
	$image = isset($section['image']) ? $section['image'] : '';
	$icon = isset($section['icon']) ? $section['icon'] : '';
	$post_type = isset($section['post_type']) ? $section['post_type'] : '';
	$columns = isset($section['columns']) ? $section['columns'] : '';
	$posts = isset($section['posts']) ? $section['posts'] : '';
	$content = isset($section['content']) ? $section['content'] : '';
	$controls = isset($section['controls']) ? $section['controls'] : '';
	$wide = isset($section['wide']) ? $section['wide'] : '';

	if ($type != '') {
		include('parts/section-'.$section['type'].'.php');
	}
}



// Hero
function hoc_hero_meta() {
	global $post;

//	wp_nonce_field(plugin_basename(__FILE__), 'sc_teams_meta_nonce');
//	require_once(dirname(__FILE__). '/meta/meta-teams.php');

	$hero = get_post_meta($post->ID, 'hero', true);

	$hidden = (isset($hero['hidden'])) ? ' checked' : '';
	$hero_text = (isset($hero["text"])) ? $hero["text"] : '';

	echo '<label for="hero-checkbox"><input type="checkbox" id="hero-checkbox" name="hero[hidden]"'.$hidden.'>Skjul hero</label>';

	wp_editor($hero_text, 'hero-editor', array(
		'media_buttons' => false,
		'textarea_rows' => 8,
		'textarea_name' => 'hero[text]',
//		'teeny' => true,
		'tinymce' => array(
			'menubar' => false,
//			'toolbar' => 'bold, italic, underline, strikethrough, bullist, numlist, alignleft, forecolor, removeformat',
			'toolbar3' => 'styleselect',
			'style_formats' => '[
				{"title":"Yellow","inline":"span","classes":"yellow"},
				{"title":"Purple","inline":"span","classes":"purple"},
				{"title":"Blue","inline":"span","classes":"blue"},
				{"title":"Orange","inline":"span","classes":"orange"},
				{"title":"Red","inline":"span","classes":"red"},
				{"title":"Gray","inline":"span","classes":"gray"},
				{"title":"Green","inline":"span","classes":"green"},
				{"title":"Dark","inline":"span","classes":"dark"}
			]'
		)
	));
}

// Author
function hoc_post_author_meta() {
	global $post;

	$employees = get_posts(array(
		'post_type' => 'employee',
		'posts_per_page' => -1
	));

	echo '<select name="hoc_post_author">';
	foreach ($employees as $employee) {
		$author_selected = ($employee->ID == get_post_meta($post->ID, 'hoc_post_author', true)) ? 'selected' : '';
		echo '<option value="">Skjult</option>';
		echo '<option value="'.$employee->ID.'" '.$author_selected.'>'.$employee->post_title.'</option>';
	}
	echo '</select>';
}

add_action('add_meta_boxes', function() {
	add_meta_box('meta-hero', 'Hero', 'hoc_hero_meta', 'page', 'normal', 'high');
	add_meta_box('meta-author', 'Forfatter', 'hoc_post_author_meta', 'post', 'side', 'high');
}, 10, 2);

add_action('save_post', function($post_id) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { return; }

//	if (wp_verify_nonce($_POST['sc_teams_meta_nonce'], plugin_basename(__FILE__))) {
//		if (!current_user_can('edit_post', $post_id) || !current_user_can('edit_page', $post_id)) {
//			return;
//		}
//	}

	$p = get_post($post_id);

	if ($p->post_type == "post") {
//		$hero = (isset($_POST['hero']) ? $_POST['hero'] : array());
		update_post_meta($post_id, 'hoc_post_author', $_POST['hoc_post_author']);
	}

	if ($p->post_type == "page") {
		$hero = (isset($_POST['hero']) ? $_POST['hero'] : array());
		update_post_meta($post_id, 'hero', $hero);
	}

	return;
});
