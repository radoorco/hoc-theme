'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('autoprefixer');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var cache = require('gulp-cache');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var sftp = require('gulp-sftp');
var del = require('del');
var concat = require('gulp-concat');
var argv = require('yargs').argv;
var gulpif = require('gulp-if');
var stripDebug = require('gulp-strip-debug');
var cssnano = require('gulp-cssnano');
var postcss = require('gulp-postcss');
var reporter = require('postcss-reporter');
var browserSync = require('browser-sync').create();
var browserify = require('browserify');
var babelify = require('babelify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var svgSprite = require('gulp-svg-sprite');
var config = require('./config');
var path = require('path');
var gutil = require('gulp-util');
var data = require('gulp-data');
var fs   = require('fs');
var csso = require('gulp-csso');
var merge = require('merge-stream');

// plumber
var onError = function(err) {
	notify.onError("Error: <%= error.message %>");
	gutil.log('An error occurred:', gutil.colors.magenta(err.message));
	this.emit('end');
};

// default task
gulp.task('default', ['serve']);

// static server + watching scss/html files
gulp.task('serve', ['sass', 'js'], function() {

	browserSync.init({
		proxy: config.tasks.browsersync.proxy,
		port: config.tasks.browsersync.port || 3000,
		reloadDelay: config.tasks.browsersync.reloadDelay,
		watchTask: true,
		debugInfo: true,
		logConnections: true,
		notify: true,
		ghostMode: {
			scroll: config.tasks.browsersync.ghostMode.scroll,
			links: config.tasks.browsersync.ghostMode.links,
			forms: config.tasks.browsersync.ghostMode.forms,
			clicks: config.tasks.browsersync.ghostMode.clicks
		},
		open: false, // Stop the browser from automatically opening
		// browser: 'google chrome', // ["google chrome", "firefox"] - Open the site in Chrome & Firefox
		reloadOnRestart: true // Reload each browser when BrowserSync is restarted.
	});

	gulp.watch('app/scss/**/*.scss', ['sass']);
	gulp.watch('app/js/**/*.js', ['js']);
	gulp.watch('app/js/vendor/*.js', ['js:vendor']);
});

// compile sass into CSS
gulp.task('sass', function() {
	var paths = {
		src: config.tasks.sass.src,
		dest: path.join(config.root.dest, config.tasks.sass.dest)
	};

	var processors = [
		autoprefixer({
			browsers: config.tasks.sass.autoprefixer.browsers,
			cascade: false
		}),
		reporter()
	];

	return gulp.src(paths.src)
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(gulpif(argv.dev, sourcemaps.init()))
		.pipe(sass({
			includePaths: require('node-bourbon').includePaths,
			errLogToConsole: true
		}))
		.pipe(postcss(processors))
		.pipe(gulpif(argv.prod, cssnano({
			safe: true,
			discardComments: {
				removeAll: true
			}
		})))
		.pipe(gulpif(argv.prod, rename({
			suffix: '.min',
			basename: 'styles'
		})))
		.pipe(gulpif(argv.dev, sourcemaps.write()))
		.pipe(gulp.dest(paths.dest))
		.pipe(gulpif(argv.ftp, sftp(config.tasks.sass.sftp)))
		.pipe(browserSync.stream())
		.pipe(notify('Sass task complete'));
});

// js
gulp.task('js', function() {
	var paths = {
		dest: path.join(config.root.dest, config.tasks.js.dest)
	};

	var bundler = browserify({
		entries: 'app/js/app.js',
		debug: true
	});

	bundler.transform('babelify', { presets: ['es2015'] });

	bundler.bundle()
		.on('error', function(err) {
			gutil.log(
				gutil.colors.red("Browserify compile error:"),
				gutil.colors.cyan(err.message)
			);

			notify({
			  'title': 'Compile Error',
			  message: err.message
			});
		})
		.pipe(source('app.js'))
		.pipe(buffer())
		.pipe(gulpif(argv.dev, sourcemaps.init()))
		.pipe(gulpif(argv.prod, stripDebug()))
		.pipe(uglify())
		.pipe(gulpif(argv.dev, sourcemaps.write()))
		.pipe(rename({
			suffix: '.min',
			basename: 'app'
		}))
		.pipe(gulpif(argv.ftp, sftp(config.tasks.js.sftp)))
		.pipe(gulp.dest(paths.dest))
		.pipe(browserSync.stream())
		.pipe(notify({
			message: 'JS task complete'
		}));
});

// vendor
gulp.task('js:vendor', function() {
	var paths = {
		dest: path.join(config.root.dest, config.tasks.js.vendor.dest)
	};

	return gulp.src(config.tasks.js.vendor.src)
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(gulpif(argv.prod, stripDebug()))
		.pipe(sourcemaps.init())
		.pipe(concat('vendor.min.js'))
		.pipe(uglify({
			mangle: false,
			outSourceMap: true
		}))
		.pipe(gulpif(argv.ftp, sftp(config.tasks.js.vendor.sftp)))
		.pipe(sourcemaps.write('/maps'))
		.pipe(gulp.dest(paths.dest))
		.pipe(browserSync.stream())
		.pipe(notify({
			message: 'Vendor task complete'
		}));
});

// sprites
gulp.task('sprites', function() {
	var paths = {
		src: config.tasks.sprites.svg.src,
		dest: path.join(config.root.dest, config.tasks.sprites.svg.dest)
	};

	return gulp.src('**/*.svg', { cwd: paths.src })
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(svgSprite({
			svg: {
				rootAttributes: {
					width: 0,
					height: 0,
					style: 'display:none'
				}
			},
			shape: {
				transform: [{
					svgo: {
						plugins: [
							{ removeStyleElement: true },
							{ removeViewBox: true },
							{ removeUselessStrokeAndFill: true }
						]
					}
				}]
			},
			mode: { defs: { dest: '.' } }
		}))
		.pipe(rename({basename: 'sprites'}))
		.pipe(gulpif(argv.ftp, sftp(config.tasks.sprites.sftp)))
		.pipe(gulp.dest(paths.dest));
});

// clean dist files
gulp.task('clean', function(cb) {
	cache.clearAll();
	del(config.tasks.clean.del, cb);
});
