<div class="row grid">

	<?php if ($post_type == 'post' && $controls == true) : ?>
		<div class="grid-controls">
			<button type="button" class="active" data-filter="all">Alle indlæg</button>
			<?php
				$categories = get_categories();

				foreach ($categories as $category) {
					if ($category->term_id != 1) {
						echo ' / <button type="button" data-filter=".'.$category->slug.'">'.$category->name.'</button>';
					}
				}
			?>
		</div>
		<div class="grid-container">
	<?php endif; ?>

	<?php $content = get_posts(array(
		'post_type' => $post_type,
		'posts_per_page' => (intval($posts) > 0) ? $posts : -1
	)); ?>

	<?php foreach ($content as $col) : ?>
		<?php
			$heading = $col->post_title;
			$text = $col->post_content;
			$link_text = 'Læs mere';

			if ($post_type == 'post') {
				$link_text = 'Læs indlægget';

				$mix_filter = 'mix';
				$post_categories = get_the_category($col->ID);
				foreach ($post_categories as $post_category) {
					$mix_filter .= ' '.$post_category->slug;
				}
			}
			if ($post_type == 'project') {
				$link_text = 'Se case';
			}
			if ($post_type == 'employee') {
				$text = get_post_meta($col->ID, 'employee_info', true)['title'];
				$link_text = 'Se profil';
			}
		?>
		<a href="<?php echo get_permalink($col->ID); ?>" class="<?php echo $mix_filter; ?> col<?php echo floor(100/$columns); ?> grid-item" style="background-image: url(<?php echo get_the_post_thumbnail_url($col->ID, 'full'); ?>);">
			<div class="grid-hover">
				<div class="grid-content">
					<?php if ($post_type == 'post') { ?>
						<div class="grid-date"><?php echo mysql2date('j M. Y', $col->post_date); ?></div>
					<?php } ?>
					<div class="grid-heading"><?php echo $col->post_title; ?></div>
					<p><?php echo substr(wp_strip_all_tags(strip_shortcodes($text), 'true'), 0, 100); ?></p>
					<div class="button"><?php echo $link_text; ?></div>
				</div>
			</div>
		</a>
	<?php endforeach; ?>

	<?php if ($post_type == 'post' && $controls == true) : ?>
		</div>
	<?php endif; ?>
</div>
