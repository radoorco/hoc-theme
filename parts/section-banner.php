<?php
	$url = wp_get_attachment_image_url($image, 'full');
?>
<div class="section-banner" style="background-image: url(<?php echo $url; ?>);">
	<div class="banner-content">
		<div class="banner-heading"><?php echo $heading; ?></div>
		<div class="banner-text"><?php echo nl2br($text); ?></div>
	</div>
</div>
