<div class="section section-text">
	<?php if(!empty($icon)) : ?>
		<div class="section-text-icon">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/img/svg/lines-<?php echo $icon; ?>.svg" alt="">
		</div>
	<?php endif; ?>

	<div class="section-text-content <?php echo ($wide) ? 'text-wide' : ''; ?>">
		<?php echo apply_filters('the_content' , $text); ?>
	</div>
</div>
