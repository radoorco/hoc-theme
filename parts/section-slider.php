<?php
	$slides = $content;

	if (empty($content)) {
		$content = get_posts(array(
			'post_type' => $post_type,
			'posts_per_page' => -1
		));

		$count = 0;
		foreach ($content as $slide) {
			$slides[$count]['image'] = get_post_thumbnail_id($slide->ID);
			$slides[$count]['heading'] = $slide->post_title;
			$slides[$count]['text'] = $slide->post_content;
			$slides[$count]['post_id'] = $slide->ID;
			$count++;
		}
	}

	$nav_text = '';
	if ($post_type == 'project') $nav_text = 'Cases';
	if ($post_type == 'testimonial') $nav_text = 'Det siger kunderne';
?>

<div class="section-slider section-<?php echo $post_type; ?>">

	<!-- Image -->
	<div class="slider-image">
		<?php foreach ($slides as $slide) : ?>
			<div class="image-slide" style="background-image: url(<?php echo wp_get_attachment_image_url($slide['image'], 'full'); ?>);"></div>
		<?php endforeach; ?>
	</div>

	<!-- Text -->
	<div class="slider-text">
		<!-- Nav -->
		<?php if (count($slides) > 1) : ?>
			<div class="slider-nav">
				<?php echo $nav_text; ?>
				<div class="slider-dots"></div>
				<div class="slider-arrows"></div>
			</div>
		<?php endif; ?>

		<?php foreach ($slides as $slide) : ?>
			<div class="text-slide">

				<?php if ($post_type == 'testimonial') : ?>

					<p class="testimonial-text">
						<?php echo $slide['text']; ?>
					</p>

					<div class="testimonial-author">
						<?php $testimonial = get_post_meta($slide['post_id'], 'testimonial', true); ?>
						<div class="testimonial-name"><?php echo $testimonial['name']; ?></div>
						<div class="testimonial-title indent"><?php echo $testimonial['title']; ?></div>
					</div>

				<?php elseif ($post_type == 'project') : ?>

					<h2 class="slide-heading"><?php echo $slide['heading']; ?></h2>
					<p><?php echo substr(wp_strip_all_tags(strip_shortcodes($slide['text']), 'true'), 0, 100); ?></p>
					<a class="button" href="<?php echo get_permalink($slide['post_id']); ?>">Se projektet</a>

				<?php else : ?>
					<h2 class="slide-heading"><?php echo $slide['heading']; ?></h2>
					<p><?php echo $slide['text']; ?></p>
				<?php endif; ?>

			</div>
		<?php endforeach; ?>
	</div>

</div>
