<?php $hero = get_post_meta($post->ID, 'hero', true); ?>
<?php if (!isset($hero['hidden'])) : ?>
	<div id="hero" role="banner" style="background-size: auto;">
		<?php if (!empty($hero['text'])) {
			the_section(array(
				'type' => 'code',
				'text' => $hero['text']
			));
		} ?>
		<a href="#main" id="tocontent">scroll<span>View.</span></a>
	</div>
<?php endif; ?>
