<footer id="footer" role="contentinfo">
	<?php echo get_option('hoc_options_twitter'); ?>
	<?php echo get_option('hoc_options_linkedin'); ?>
	<?php echo get_option('hoc_options_facebook'); ?>
	<?php echo get_option('hoc_options_instagram'); ?>

	<div class="row footer-content">
		<div class="col33">
			<h4>House of Code:</h4>
			<p>Kochsgade 31D, 3.<br>5000 Odense C</p>
			<p>Mejlgade 27, 1. TH<br>8000 Aarhus C</p>
			<p>CVR: 36030410</p>
		</div>

		<div class="col33">
			<h4>Start et projekt:</h4>
			<p>
				<a href="mailto:info@houseofcode.io">info@houseofcode.io</a><br>+45 72 170 220
			</p>
		</div>

		<div class="col33">
			<h4>Socialiser:</h4>
			<ul>
				<li><a href="">Twitter</a></li>
				<li><a href="">LinkedIn</a></li>
				<li><a href="">Facebook</a></li>
				<li><a href="">Instagram</a></li>
			</ul>
		</div>
	</div>

	<div class="footer-language">
		<a href="">Dansk</a> / <a href="">English</a>
	</div>

	<div id="footer-logo"></div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
