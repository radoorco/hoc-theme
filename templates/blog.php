<?php
/*
	Template Name: Blog
*/
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('parts/hero'); ?>

	<main id="main">
		<?php the_sections($post->ID); ?>
	</main>
<?php endwhile; ?>

<?php get_footer(); ?>
