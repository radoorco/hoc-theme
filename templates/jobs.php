<?php
/*
	Template Name: Jobs
*/
?>

<?php get_header(); ?>

<?php get_template_part('parts/hero'); ?>

<main id="main">
<?php
	$jobs = get_posts(array(
		'post_type' => 'job',
		'posts_per_page' => -1,
		'orderby'	=> 'meta_value_num date',
		'order'		=> 'DESC',
		'meta_key'	=> 'job_highlight'
	));

	foreach ($jobs as $job) {
		$highlight = get_post_meta($job->ID, 'job_highlight', true);

		echo ($highlight == 1) ? '<div class="job-highlight clip" style="background-image: url('.get_the_post_thumbnail_url($job->ID, 'full').');">' : '<div class="section">';

		?>
			<div class="section-text">
				<div class="section-text-icon">
					<img src="<?php echo get_template_directory_uri(); ?>/dist/img/svg/lines-plus.svg" alt="">
				</div>

				<div class="section-text-content">
					<h2><?php echo $job->post_title; ?></h2>
					<p><?php echo substr(wp_strip_all_tags(strip_shortcodes($job->post_content), 'true'), 0, 150); ?></p>
					<a class="button" href="<?php echo get_permalink($job->ID); ?>">Læs hele jobopslaget</a>
				</div>
			</div>
		</div>
		<?php
	}

	the_sections($post->ID);
?>
</main>

<?php get_footer(); ?>
