<?php
/*
	Template Name: About
*/
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('parts/hero'); ?>

	<main id="main">
		<?php $image = (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail_url($post->ID, 'full') : ''; ?>
		<div id="hero" class="clip" style="background-image: url(<?php echo $image; ?>);">
			<div class="section-banner">
				<div class="banner-heading">Vi er House of Code</div>
				<div class="banner-text">Vi laver apps og har det sjovt imens</div>
			</div>
		</div>

		<?php the_sections($post->ID); ?>

		<div class="section section-grid employees">
			<h2>Medarbejdere</h2>
			<p>Mød holdet. Vi styrker sammenholdet gennem plads til forskellighed.</p>

			<?php $employees = get_posts(array(
				'post_type' => 'employee',
				'posts_per_page' => -1
			)); ?>

			<div class="row grid">
				<?php foreach ($employees as $employee) : ?>
					<?php $employee_info = get_post_meta($employee->ID, 'employee_info', true); ?>
					<a href="<?php echo get_permalink($employee->ID); ?>" class="col33 grid-item" style="background-image: url(<?php echo get_the_post_thumbnail_url($employee->ID, 'full'); ?>);">
						<div class="grid-hover">
							<div class="grid-content">
								<div class="grid-icons">
									<?php if ($employee_info['phone']) echo '<div class="icon icon-phone">'.$employee_info['phone'].'</div>'; ?>
									<?php if ($employee_info['email']) echo '<div class="icon icon-mail">'.$employee_info['email'].'</div>'; ?>
								</div>
								<div class="grid-heading"><?php echo $employee->post_title; ?></div>
								<p><?php echo $employee_info['title']; ?></p>
								<div class="button">Se profil</div>
							</div>
						</div>
					</a>
				<?php endforeach; ?>
			</div>
		</div>

		<div class="section section-grid facts">
			<h2>Følg med</h2>
			<p>House of Code i tal og billeder. Random facts og Instagram-posts.</p>

			<?php $facts = get_post_meta($post->ID, 'facts', true); ?>

			<div class="row grid">
				<?php foreach ($facts as $fact) : ?>
					<div class="col33 grid-item <?php echo $fact['color']; ?>" style="background-image: url(<?php echo wp_get_attachment_image_url($fact['image'], 'full'); ?>);">
						<div class="fact-content">
							<div class="fact-heading"><?php echo nl2br($fact['heading']); ?></div>
							<div class="fact-text"><?php echo nl2br($fact['text']); ?></div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</main>
<?php endwhile; ?>

<?php get_footer(); ?>
