<?php
/*
	Template Name: Presse
*/
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('parts/hero'); ?>

	<main id="main">
		<?php the_sections($post->ID); ?>

		<?php $press_materials = get_post_meta($post->ID, 'press', true); ?>

		<?php
			$press1 = array();
			$press2 = array();
			$press3 = array();

			foreach ($press_materials as $press_material) {
				if ($press_material['category'] == 1) {
					array_push($press1, $press_material);
				}
				if ($press_material['category'] == 2) {
					array_push($press2, $press_material);
				}
				if ($press_material['category'] == 3) {
					array_push($press3, $press_material);
				}
			}
		?>

		<?php if (!empty($press1)) : ?>
			<div class="section section-grid">
				<h2>Logoer</h2>
				<div class="row grid">
					<?php foreach ($press1 as $press) : ?>
						<a href="<?php echo wp_get_attachment_url($press['file']); ?>" target="_blank" class="col33 grid-item" style="background-image: url(<?php echo wp_get_attachment_image_url($press['image'], 'full'); ?>);">
							<div class="grid-content">
								<div class="grid-heading"><?php echo nl2br($press['text']); ?></div>
							</div>
						</a>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if (!empty($press2)) : ?>
			<div class="section section-grid">
				<h2>Kontorbilleder</h2>
				<div class="row grid">
					<?php foreach ($press2 as $press) : ?>
						<a href="<?php echo wp_get_attachment_url($press['file']); ?>" target="_blank" class="col33 grid-item" style="background-image: url(<?php echo wp_get_attachment_image_url($press['image'], 'full'); ?>);">
							<div class="grid-content">
								<div class="grid-heading"><?php echo nl2br($press['text']); ?></div>
							</div>
						</a>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if (!empty($press3)) : ?>
			<div class="section section-grid">
				<h2>Partnerbilleder</h2>
				<div class="row grid">
					<?php foreach ($press3 as $press) : ?>
						<a href="<?php echo wp_get_attachment_url($press['file']); ?>" target="_blank" class="col33 grid-item" style="background-image: url(<?php echo wp_get_attachment_image_url($press['image'], 'full'); ?>);">
							<div class="grid-content">
								<div class="grid-heading"><?php echo nl2br($press['text']); ?></div>
							</div>
						</a>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>
	</main>
<?php endwhile; ?>

<?php get_footer(); ?>
