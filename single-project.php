<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

	<?php $gallery = get_post_meta($post->ID, 'gallery_slides', true); ?>

	<?php if (!empty($gallery)) : ?>
		<div class="small-gallery clip">
		<div class="section gallery">
			<h2><?php the_title(); ?></h2>

			<?php $slides = get_post_meta($post->ID, 'gallery_slides', true); ?>
			<div class="section-slider">
				<!-- Image -->
				<div class="slider-image">
					<?php foreach ($slides as $slide) : ?>
						<div class="image-slide" style="background-image: url(<?php echo wp_get_attachment_image_url($slide['image'], 'full'); ?>);"></div>
					<?php endforeach; ?>
				</div>

				<!-- Text -->
				<div class="slider-text">
					<!-- Nav -->
					<?php if (count($slides) > 1) : ?>
						<div class="slider-nav">
							<div class="slider-dots"></div>
							<div class="slider-arrows"></div>
						</div>
					<?php endif; ?>

					<?php foreach ($slides as $slide) : ?>
						<div class="text-slide">
							<h2 class="slide-heading"><?php echo $slide['heading']; ?></h2>
							<p><?php echo $slide['text']; ?></p>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		</div>
	<?php else : ?>

	<div id="hero" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>);">
		<div class="section-banner">
			<?php /* <div class="banner-date"><?php echo mysql2date('j M. Y', $post->post_date); ?></div> */ ?>
			<div class="banner-heading"><?php the_title(); ?></div>
			<div class="banner-text"><?php if (has_excerpt()) echo get_the_excerpt(); ?></div>
		</div>
	</div>

	<main class="section section-content" role="main">
		<?php the_content(); ?>
	</main>

	<?php the_section(array(
		'type' => 'grid',
		'post_type' => 'project',
		'columns' => 3,
		'posts' => 3
	)); ?>

	<?php the_section(array(
		'type' => 'slider',
		'post_type' => 'testimonial'
	)); ?>

	<?php
//	echo get_previous_post_link();
//	echo get_next_post_link();
	?>

	<?php
		$next_post = get_next_post();

		if (empty($next_post)) {
			$next_post = get_posts(array(
				'post_type' => 'project',
				'posts_per_page' => 1,
				'order' => 'ASC'
			));
			$next_post = $next_post[0];
		}
	?>
	<a href="<?php echo get_permalink($next_post->ID); ?>" class="section-banner next-project" style="background-image: url(<?php echo get_the_post_thumbnail_url($next_post->ID, 'full'); ?>);">
		<div class="banner-heading">Næste case ></div>
		<div class="banner-text"><?php echo $next_post->post_title; ?></div>
	</a>

	<?php endif; ?>

<?php endwhile; ?>

<?php get_footer(); ?>
