<?php
/**
 * responsive vimeo/youtube embed
 *
 * @return string html block
 */
function responsive_wrap_oembed_dataparse( $html, $data ) {
 // verify oembed data (as done in the oEmbed data2html code)
 if ( ! is_object( $data ) || empty( $data->type ) )
 return $html;

 // verify that it is a video
 if ( !($data->type == 'video') )
 return $html;

// dont display suggested videos at the end
if ($data->provider_name == 'YouTube') {
    $html = str_replace('feature=oembed', 'feature=oembed&#038;rel=0', $html);
}

 // calculate aspect ratio
 $ar = $data->height / $data->width;

 // set the aspect ratio modifier
 $ar_mod = ( abs($ar-(4/3)) < abs($ar-(16/9)) ? 'rc-embed-responsive-4by3' : 'rc-embed-responsive-16by9');

 // strip width and height from html
 $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );

 // Return code
 return '<div class="video-container '. $ar_mod .' rc-embed-responsive rc-oembed-js" data-aspectratio="'. number_format($ar, 4, '.', '') .'">'.$html.'</div>';
}

// Hook onto 'oembed_dataparse' and get 2 parameters
add_filter( 'oembed_dataparse','responsive_wrap_oembed_dataparse',10,2);

// So, turns out that the oEmbed html is cached by WP. This makes sense – we don’t want to ask for and process oEmbed data for every page call. So, for now, you have to update all posts/pages that has oEmbed to cache the new data or delete the oEmbed postmeta.
