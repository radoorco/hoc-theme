<?php

/**
 * yoast seo breadcrumbs
 * Common places where you could place your breadcrumbs are inside your
 * single.php and/or page.php file just above the page's title.
 * Another option that makes it really easy in some themes is
 * by just pasting the code in header.php at the very end.
 * https://kb.yoast.com/kb/implement-wordpress-seo-breadcrumbs/
 *
 * @return String display breadcrumbs
 */
function rc_yoast() {
    if ( function_exists('yoast_breadcrumb') ) {
        return yoast_breadcrumb('<p id="breadcrumbs">','</p>');
    }
}
