<?php

/**
 * load jquery from jquery's cdn with a local fallback
 */
function register_jquery() {
    $jquery_version = wp_scripts()->registered['jquery']->ver;
    wp_deregister_script('jquery');
    wp_register_script(
        'jquery',
        '//ajax.googleapis.com/ajax/libs/jquery/' . $jquery_version . '/jquery.min.js',
        [],
        null,
        true
    );

    add_filter('script_loader_src', 'jquery_local_fallback', 10, 2);
}
add_action('wp_enqueue_scripts', 'register_jquery', 100);

/**
 * output the local fallback immediately after jquery's <script>
 * @link http://wordpress.stackexchange.com/a/12450
 */
function jquery_local_fallback($src, $handle = null) {
    static $add_jquery_fallback = false;

    if ($add_jquery_fallback) {
        echo '<script>window.jQuery || document.write(\'<script src="' . $add_jquery_fallback .'"><\/script>\')</script>' . "\n";
        $add_jquery_fallback = false;
    }

    if ($handle === 'jquery') {
        $add_jquery_fallback = apply_filters('script_loader_src', \includes_url('/js/jquery/jquery.js'), 'jquery-fallback');
    }

    return $src;
}
add_action('wp_head','jquery_local_fallback');
