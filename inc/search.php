<?php
/**
 * redirect to if search result return one post
 */
function redirect_single_post() {
    if (is_search()) {
        global $wp_query;

        if ($wp_query->post_count == 1 && $wp_query->max_num_pages == 1) {
            wp_redirect( get_permalink( $wp_query->posts['0']->ID ) );
            exit;
        }
    }
}
// add_action('template_redirect', 'redirect_single_post');

/**
 * exclude pages from wordpress search results
 */
function filter_search($query) {
    if ($query->is_search) {
        $query->set('post_type', 'post');
    }
    return $query;
}
// add_filter('pre_get_posts', 'filter_search');

/**
 * redirects search results from /?s=query to /search/query/, converts %20 to +
 */
function rc_redirect() {
    global $wp_rewrite;

    if (!isset($wp_rewrite) || !is_object($wp_rewrite) || !$wp_rewrite->get_search_permastruct()) {
        return;
    }

    $search_base = $wp_rewrite->search_base;

    if (is_search() && !is_admin() && strpos($_SERVER['REQUEST_URI'], "/{$search_base}/") === false && strpos($_SERVER['REQUEST_URI'], '&') === false) {
        wp_redirect(get_search_link());
        exit();
    }
}
add_action('template_redirect','rc_redirect');

function rc_rewrite($url) {
    return str_replace('/?s=', '/search/', $url);
}
add_filter('wpseo_json_ld_search_url', 'rc_rewrite');
