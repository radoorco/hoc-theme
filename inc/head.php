<?php
/**
 * wp_head()
 *
 * Color address mobile bar
 * Add favicons
 * Add manifest
 */


/**
 * notes
 * On Android, the color you set above is applied to Google Chrome address
 * bar and darker 700 tint of same color is applied to Android navigation bar
 * every time someone visits your website.
 * msapplication-navbutton-color meta tag also works for desktop IE pinned tabs.
 * on Safari, you cannot specify the exact color, but instead you can use:
 * - default – the status bar appears normal.
 * - black – the status bar has a black background.
 * - black-translucent – the status bar is black and translucent.
 */
function address_mobile_address_bar() {
    $color = "#262626";
    //this is for Chrome, Firefox OS, Opera and Vivaldi
    echo '<meta name="theme-color" content="'.$color.'">';
    //Windows Phone **
    echo '<meta name="msapplication-navbutton-color" content="'.$color.'">';
    // iOS Safari
    echo '<meta name="apple-mobile-web-app-capable" content="yes">';
    echo '<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">';
}

/**
 * add favicon
 */
function rc_add_favicon() {
//    echo '<link rel="shortcut icon" href="' . get_stylesheet_directory_uri() .'/favicon.ico"/>';
//    echo '<link rel="icon" href="' . get_stylesheet_directory_uri() .'/favicon.ico" type=”image/x-icon”/>';
//    echo '<link rel="apple-touch-icon" href="'. get_stylesheet_directory_uri() .'/apple-touch-icon.png">';
}

/**
 * add manifest.json
 */
function rc_add_manifest() {
    echo '<link rel="manifest" href="'. get_template_directory_uri() .'/manifest.json">';
}
