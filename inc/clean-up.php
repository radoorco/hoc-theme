<?php
/**
* Various clean ups
*
* Remove unnecessary <link>'s
* Remove inline CSS and JS from WP emoji support
* Remove inline CSS used by Recent Comments widget
* Remove inline CSS used by posts with galleries
* Remove self-closing tag and change ''s to "'s on rel_canonical()
* Don't return the default description in the rss feed if it hasn't been changed
* Remove the p from around imgs
* Stop wordpress automatically adding br
* Remove comments from certain post types
* Remove fields from comment form
* Remove width and height attributes from images
*/

/**
 *  remove unnecessary <link>'s
 */
function head_cleanup() {
    // category feeds
    remove_action( 'wp_head', 'feed_links_extra', 3 );
    // post and comment feeds
    remove_action( 'wp_head', 'feed_links', 2 );
    // EditURI link
    remove_action( 'wp_head', 'rsd_link' );
    // windows live writer
    remove_action( 'wp_head', 'wlwmanifest_link' );
    // previous link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
    // start link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    // links for adjacent posts
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    // WP version
    remove_action( 'wp_head', 'wp_generator' );

    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    remove_action('wp_head', 'wp_oembed_add_discovery_links');
    remove_action('wp_head', 'wp_oembed_add_host_js');

    // remove WP version from css
    add_filter( 'style_loader_src', 'remove_wp_ver_css_js', 9999 );
    // remove Wp version from scripts
    add_filter( 'script_loader_src', 'remove_wp_ver_css_js', 9999 );

    // filters for wp-api version 1.x
    add_filter('json_enabled', '__return_false');
    add_filter('json_jsonp_enabled', '__return_false');

    // filters for wp-api version 2.x
    add_filter('rest_enabled', '__return_false');
    add_filter('rest_jsonp_enabled', '__return_false');

    // remove rest api info from head and headers
    remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'template_redirect', 'rest_output_link_header', 11 );
}

/**
 * remove query strings from static resources
 */
function remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

function rc_rel_canonical() {
    global $wp_the_query;

    if (!is_singular()) {
        return;
    }

    if (!$id = $wp_the_query->get_queried_object_id()) {
        return;
    }

    $link = get_permalink($id);
    echo "\t<link rel=\"canonical\" href=\"$link\">\n";
}

/**
 * disable emoji styles & script
 */
function rc_remove_emojicons() {
    // Remove from comment feed and RSS
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    // Remove from emails
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

    // Remove from head tag
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

    // Remove from print related styling
    remove_action( 'wp_print_styles', 'print_emoji_styles' );

    // Remove from admin area
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
}

function disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    }
    else {
        return array();
    }
}

/**
 * clean up output of stylesheet <link> tags
 */
function rc_clean_style_tag($input) {
    preg_match_all("!<link rel='stylesheet'\s?(id='[^']+')?\s+href='(.*)' type='text/css' media='(.*)' />!", $input, $matches);

    if (empty($matches[2])) {
        return $input;
    }
    // Only display media if it is meaningful
    $media = $matches[3][0] !== '' && $matches[3][0] !== 'all' ? ' media="' . $matches[3][0] . '"' : '';

    return '<link rel="stylesheet" href="' . $matches[2][0] . '"' . $media . '>' . "\n";
}
add_filter('style_loader_tag', 'rc_clean_style_tag');

/**
 * clean up output of <script> tags
 */
function rc_clean_script_tag($input) {
  $input = str_replace("type='text/javascript' ", '', $input);

  return str_replace("'", '"', $input);
}
add_filter('script_loader_tag', 'rc_clean_script_tag');

/**
 * add and remove body_class() classes
 */
function rc_body_class($classes) {
    // Add post/page slug if not present
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    // Remove unnecessary classes
    $home_id_class = 'page-id-' . get_option('page_on_front');
    $home_id_class = 'page-id-' . get_option('page_on_front');
    $remove_classes = [
        'page-template-default',
        $home_id_class
    ];

    $classes = array_diff($classes, $remove_classes);

    return $classes;
}
add_filter('body_class', 'rc_body_class');

/**
 * remove unnecessary self-closing tags
 */
function remove_self_closing_tags($input) {
    return str_replace(' />', '>', $input);
}
add_filter('get_avatar', 'remove_self_closing_tags'); // <img />
add_filter('comment_id_fields', 'remove_self_closing_tags'); // <input />
add_filter('post_thumbnail_html', 'remove_self_closing_tags'); // <img />

/**
 * don't return the default description in the rss feed if it hasn't been changed
 */
function remove_default_description($bloginfo) {
    $default_tagline = 'Just another WordPress site';
    return ($bloginfo === $default_tagline) ? '' : $bloginfo;
}
add_filter('get_bloginfo_rss', 'remove_default_description');

/**
 * remove the p from around imgs
 * @link http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/
 */
function filter_ptags_on_images($content) {
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .*>)\s*(<\/a>)?\s*<\/p>/i', '\1\2\3', $content);
}

/**
 * stop wordpress automatically adding br
 */
function rc_wpautop_without_br( $content ) {
    return wpautop( $content, false );
}

// Remove the default filters
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

// Add the new function to be the the_content and the_excerpt filter
add_filter( 'the_content', 'rc_wpautop_without_br' , 99);
add_filter( 'the_excerpt', 'rc_wpautop_without_br' , 99);

/**
 * remove comments from certain post types
 */
function remove_comment_from_post_type( ) {
    remove_post_type_support('custom-post-type', 'comments');
    remove_post_type_support('custom-post-type', 'trackbacks');
}
add_filter( 'init', 'remove_comment_from_post_type' );

/**
 * remove fields from comment form
 */
function remove_comment_fields($fields) {
    unset($fields['url']);
    return $fields;
}
// add_filter('comment_form_default_fields','remove_comment_fields');

/**
 * remove width and height attributes from images
 */
function remove_width_and_height_attribute( $html ) {
   return preg_replace( '/(height|width)="\d*"\s/', "", $html );
}
add_filter( 'get_image_tag', 'remove_width_and_height_attribute', 10 );
add_filter( 'post_thumbnail_html', 'remove_width_and_height_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_and_height_attribute', 10 );
