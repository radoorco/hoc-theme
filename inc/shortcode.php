<?php

function email_encode( $atts, $email ){
    return '<a href="mailto:'.antispambot($email).'">'.antispambot($email).'</a>';
}

add_shortcode( 'email', 'email_encode' );
