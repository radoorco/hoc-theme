<?php
/**
 * enqueue scripts and styles.]
 */
function rc_scripts() {
	global $post, $wp_query;

	/*----------  load js  ----------*/

	wp_register_script('rc-theme-vendor', get_template_directory_uri() . '/dist/js/vendor.min.js', array('jquery'), '1.0.0', true);
	wp_register_script('rc-theme-script', get_template_directory_uri() . '/dist/js/app.min.js', array('jquery'), '1.0.0', true);
	wp_register_script('rc-bodymovin', get_template_directory_uri() . '/dist/js/bodymovin_light.min.js');
	wp_register_script('rc-mixitup', get_template_directory_uri() . '/dist/js/mixitup.min.js', '', '', true);
	wp_register_script('rc-jquery-ui', get_template_directory_uri() . '/dist/js/jquery-ui-1.10.4.custom.min.js', array('jquery'), '', true);
	wp_register_script('rc-clipthru', get_template_directory_uri() . '/dist/js/jq-clipthru.min.js', array('rc-jquery-ui'), '', true);

	wp_enqueue_script('rc-theme-vendor');
	wp_enqueue_script('rc-theme-script');
	wp_enqueue_script('rc-bodymovin');
	wp_enqueue_script('rc-mixitup');
	wp_enqueue_script('rc-jquery-ui');
	wp_enqueue_script('rc-clipthru');

	/*----------  inline js  ----------*/

	// custom js
	$custom_js = get_theme_mod('rc_custom_theme_js');
	if ( !empty( $custom_js ) ) {
		wp_add_inline_script( 'rc-theme-script', $custom_js, 'before' );
	}

	/*----------  typekit  ----------*/

	// $custom_js = "try{Typekit.load({ async: true });}catch(e){}";
	// wp_add_inline_script( 'rc-theme-script', $custom_js, 'after');




	/*----------  load css  ----------*/

	wp_enqueue_style( 'rc-theme-style', get_template_directory_uri() . '/dist/css/main.css' );

	/*----------  custom css  ----------*/

	/**
	 * add custom css files
	 * file must located in dist/css/
	 * rc_custom_theme_css_file must be eneabled
	 *
	 */
	$locate_custom_css = locate_template( 'dist/css/custom-main.css' );
	$custom_css_file = get_theme_mod('rc_custom_theme_css_file');
	if ( ($locate_custom_css != '' && !empty($custom_css_file)) ) {
		wp_enqueue_style( 'rc-theme-custom-style', get_template_directory_uri() . '/app/css/custom-main.css' );
	}

	/*----------  inline css  ----------*/

	// custom css
	$custom_css = get_theme_mod('rc_custom_theme_css');
	if ( !empty( $custom_css ) ) {
		wp_add_inline_style( 'rc-theme-style', $custom_css, 'before' );
	}

	// load comments
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/*----------  ajax  ----------*/

	wp_localize_script('rc-theme-script', 'ajax_object',
		array(
			'ajax_url' => admin_url('admin-ajax.php'),
			'home_url' => get_home_url(),
			'sprite_url' => get_template_directory_uri().'/dist/svg/sprites.svg'
		)
	);
}

/**
 * add editor styling
 */
function rc_custom_editor_styles() {
  add_editor_style('dist/css/editor-style.css' );
}

/**
 * allow HTML to be placed in your emails
 */
function set_html_content_type() {
	return 'text/html';
}
add_filter( 'wp_mail_content_type', 'set_html_content_type' );

/**
 * add custom thumbnails
 */
function rc_theme_support() {
	// wp thumbnails (sizes handled in functions.php)
	add_theme_support( 'post-thumbnails' );

	// custom image size
	// add_image_size( 'components-featured-image', 640, 9999 );
	// add_image_size( 'components-hero', 1280, 1000, true );
	// add_image_size( 'components-thumbnail-avatar', 100, 100, true );

	// default thumb size
	set_post_thumbnail_size(125, 125, true);

	// load text domain
	load_theme_textdomain( 'rc', get_template_directory() . '/languages' );

	// switch default core markup for search form, comment form, and comments
	// to output valid HTML5
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
}

/**
 * allow svg through wordpress media uploader
 */
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * disable responsive images
 */
// add_filter( 'max_srcset_image_width', create_function( '', 'return 1;' ) );

/**
 *  adds instruction text after the post title input
 */
function rc_edit_form_after_title() {
	$tip = '<strong>TIP:</strong> To create a single line break use SHIFT+RETURN. By default, RETURN creates a new paragraph.';
	echo '<p style="margin-bottom:0;">'.$tip.'</p>';
}
// add_action('edit_form_after_title', 'rc_edit_form_after_title');
