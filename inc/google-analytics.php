<?php
/**
 * google analytics snippet from html5 boilerplate
 *
 */

function rc_google_analytics() {
    $ga_key = '';
    $ga_script = "
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', '. $ga_key .', 'auto');
          ga('send', 'pageview');
    ";

    if ( !empty( $ga_key ) ) {
        wp_add_inline_script( 'rc-theme-script', $ga_script, 'before' );
    }
}
add_action('wp_enqueue_scripts', 'rc_google_analytics', 20);
