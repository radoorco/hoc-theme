<?php
/**
 * allows query vars to be added, removed, or changed prior to executing the query
 */
function rc_query_vars( $qvars ) {
  $qvars[] = 'email';
  $qvars[] = 'token';

  return $qvars;
}
add_filter( 'query_vars', 'rc_query_vars' , 10, 1 );
