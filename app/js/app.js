//import example from './lib/example.js';

jQuery(document).ready(function($) {

	// Slick
	var sliders = ['project', 'testimonial', 'employee']

	// Slider Image
	$.each(sliders, function(i) {
		$('.section-'+sliders[i]+' .slider-image').slick({
			arrows: false,
			slide: '.image-slide',
			fade: true,
			asNavFor: '.section-'+sliders[i]+' .slider-text',
			customPaging : function(slider, i) {
				var count = i+1;
				if (count < 10) {
					count = '0'+count;
				}
				return '<button type="button" data-role="none" role="button" tabindex="0">'+count+'</button>';
			}
		});
	});

	// Slider Text
	$.each(sliders, function(i) {
		$('.section-'+sliders[i]+' .slider-text').slick({
			appendDots: '.section-'+sliders[i]+' .slider-dots',
			appendArrows: '.section-'+sliders[i]+' .slider-arrows',
			dots: true,
			arrows: true,
			slide: '.text-slide',
			fade: true,
			asNavFor: '.section-'+sliders[i]+' .slider-image',
			customPaging : function(slider, i) {
				var count = i+1;
				if (count < 10) {
					count = '0'+count;
				}
				return '<button type="button" data-role="none" role="button" tabindex="0">'+count+'</button>';
			}
		});
	});


	// Mobile menu
	$('.mobile-toggle').click(function() {
		$(this).toggleClass('active');
		$('.header-nav').toggleClass('active');
	});


	// MixItUp filter
	if ($('.grid-container').length > 0) {
		var mixer = mixitup('.grid-container');
	}

	// Scroll to
	$('#tocontent').click(function(e) {
		e.preventDefault();

		$('html, body').animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 400);
	});


	// jq-clipthru
	// https://github.com/salsita/jq-clipthru
	$('#header-nav').clipthru({
		autoUpdate: true,
		autoUpdateInterval: 100,
		blockSource: {
			'clone-class': ['.clip', '.section-banner', '.section-slider', '.grid-item', '#footer']
		}
	});


	// BodyMovin
	/*
	var animData = {
		container: document.getElementById('bodymovin'),
		renderer: 'svg',
		loop: false,
		autoplay: false,
		autoloadSegments: true,
		rendererSettings: {
			progressiveLoad: false
		},
		path: '../wp-content/themes/hoc/dist/data.json'
	};
	anim = bodymovin.loadAnimation(animData);
	*/
});
/*
var anim;
var wrapStart = 0;
var wrapEnd = 0;
var wrapHeight = 0;
var winHeight = 0;
var wrapPro = 0;

jQuery(window).resize(function() {
	var first = jQuery('#main .section-text:first');
	var last = jQuery('#main .section-text:last');
	wrapStart = first.offset().top;
	wrapEnd = last.offset().top+last.outerHeight();
	wrapHeight = wrapEnd - wrapStart;
	wrapPro = wrapHeight/100;
	winHeight = jQuery(window).outerHeight();
}).resize();

jQuery(window).scroll(function() {
	var winTop = jQuery(window).scrollTop();

	var begin = (winTop + (winHeight / 2));
	if (wrapStart < begin && wrapEnd > winTop + winHeight / 2) {
		var indicator = Math.abs(((begin-wrapStart)/wrapPro)/100);
		console.log(indicator);
		anim.updateAnimation(indicator);
	}
});
*/
