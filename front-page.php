<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('parts/hero'); ?>

	<main id="main">
		<?php the_sections($post->ID); ?>

		<?php $blogs = get_posts(array(
			'posts_per_page' => 1
		)); ?>

		<div class="section section-text">
			<div class="section-text-icon">
				<img src="<?php echo get_template_directory_uri(); ?>/dist/img/svg/lines-bubble.svg" alt="">
			</div>

			<div class="section-text-content">
				<h2>Seneste <span class="purple">blogindlæg</span></h2>
				<div class="blog-entry">
					<?php foreach ($blogs as $blog) : ?>
						<div class="sub-date"><?php echo mysql2date('j M. Y', $blog->post_date); ?></div>
						<h2><?php echo $blog->post_title; ?></h2>
						<p><?php echo substr(wp_strip_all_tags(strip_shortcodes($blog->post_content), 'true'), 0, 100); ?></p>
						<a href="<?php echo get_permalink($blog->ID); ?>" class="button">Læs indlægget</a>
					<?php endforeach; ?>
				</div>
				<a href="/blog-nyheder/" class="button">Se flere blogindlæg</a>
			</div>
		</div>
	</main>
<?php endwhile; ?>

<?php the_section(array(
	'type' => 'grid',
	'post_type' => 'project',
	'columns' => 3,
	'posts' => 3
)); ?>

<?php get_footer(); ?>
