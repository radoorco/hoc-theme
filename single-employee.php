<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

	<div class="section gallery">
		<h2>Medarbejdere</h2>

		<?php $slides = get_post_meta($post->ID, 'employee_slides', true); ?>
		<div class="section-slider section-employee">
			<!-- Image -->
			<div class="slider-image">
				<?php foreach ($slides as $slide) : ?>
					<div class="image-slide" style="background-image: url(<?php echo wp_get_attachment_image_url($slide['image'], 'full'); ?>);"></div>
				<?php endforeach; ?>
			</div>

			<!-- Text -->
			<div class="slider-text">
				<!-- Nav -->
				<?php if (count($slides) > 1) : ?>
					<div class="slider-nav">
						<div class="slider-dots"></div>
						<div class="slider-arrows"></div>
					</div>
				<?php endif; ?>

				<?php foreach ($slides as $slide) : ?>
					<div class="text-slide">
						<h2 class="slide-heading"><?php echo $slide['heading']; ?></h2>
						<p><?php echo $slide['text']; ?></p>
					</div>
				<?php endforeach; ?>
			</div>

			<div class="employee-info">
				<?php $employee_info = get_post_meta($post->ID, 'employee_info', true); ?>
				<ul>
					<?php
						if (!empty($employee_info['title'])) echo '<li class="icon-title">'.$employee_info['title'].'</li>';
						if (!empty($employee_info['phone'])) echo '<li class="icon-phone">'.$employee_info['phone'].'</li>';
						if (!empty($employee_info['email'])) echo '<li class="icon-mail"><a href="mailto:'.$employee_info['email'].'">'.$employee_info['email'].'</a></li>';
						if (!empty($employee_info['skype'])) echo '<li class="icon-skype">'.$employee_info['skype'].'</li>';
						if (!empty($employee_info['twitter'])) echo '<li class="icon-twitter"><a href="https://twitter.com/'.$employee_info['twitter'].'" target="_blank">@'.$employee_info['twitter'].'</a></li>';
						if (!empty($employee_info['instagram'])) echo '<li class="icon-instagram"><a href="https://www.instagram.com/'.$employee_info['instagram'].'" target="_blank">@'.$employee_info['instagram'].'</a></li>';
						if (!empty($employee_info['linkedin'])) echo '<li class="icon-linkedin"><a href="https://dk.linkedin.com/in/'.$employee_info['linkedin'].'" target="_blank">@'.$employee_info['linkedin'].'</a></li>';
						if (!empty($employee_info['dribbble'])) echo '<li class="icon-dribbble"><a href="https://dribbble.com/'.$employee_info['dribbble'].'" target="_blank">@'.$employee_info['dribbble'].'</a></li>';
					?>
				</ul>
			</div>
		</div>
	</div>
<?php endwhile; ?>

<?php get_footer(); ?>
